from django.shortcuts import render
from datetime import datetime, date
from django.http.response import HttpResponse
from .models import Note
from django.core import serializers

mhs_name = "Alya Azhar Agharid"
def index(request):
    Notes = Note.objects.all().values() 
    response = {'name':mhs_name,'Notes': Notes}
    return render(request, 'lab2.html', response)
    
def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    NotesJSon = Note.objects.all().values()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")