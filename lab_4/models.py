from django.db import models

class Note(models.Model):
    sender = models.CharField(max_length=200)
    receiver = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    message = models.TextField(max_length=1000)