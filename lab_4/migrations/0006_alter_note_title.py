# Generated by Django 3.2.7 on 2021-10-06 18:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0005_alter_note_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='title',
            field=models.CharField(max_length=200),
        ),
    ]
