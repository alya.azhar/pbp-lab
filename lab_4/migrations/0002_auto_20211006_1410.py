# Generated by Django 3.2.7 on 2021-10-06 07:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('receiver', models.CharField(max_length=30)),
                ('sender', models.CharField(max_length=30)),
                ('title', models.CharField(max_length=200)),
                ('message', models.CharField(max_length=1000)),
            ],
        ),
        migrations.DeleteModel(
            name='Friend',
        ),
    ]
