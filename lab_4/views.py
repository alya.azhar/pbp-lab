from django.http import response
from django.http.request import HttpRequest
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from .models import Note
from .forms import NoteForm

mhs_name = "Azhar"

@login_required(login_url='/admin/login/')
def index(request):
    Notes = Note.objects.all().values() 
    response = {'name':mhs_name,'Notes': Notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    form = NoteForm
    response = {'name':mhs_name,'form':form}
    if request.method == 'POST':
        noteForm = NoteForm(request.POST)
        if noteForm.is_valid():
            noteForm.save()
            return redirect('/lab-4')
    return render(request, 'lab4_form.html',response)

@login_required(login_url='/admin/login/')
def note_list(request):
    Notes = Note.objects.all().values() 
    response = {'name':mhs_name,'Notes': Notes}
    return render(request, 'lab4_note_list.html', response)