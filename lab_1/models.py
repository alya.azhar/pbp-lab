from django.db import models
from datetime import date, datetime
from django.db.models.fields import DateField

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    DOB = models.DateField()
