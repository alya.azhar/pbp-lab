import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';

class AddForumPage extends StatelessWidget {
  TextEditingController field = new TextEditingController();
  String pasteValue = "";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'CASEWORQER',
        theme: ThemeData(
          primarySwatch: primaryBlack,
          scaffoldBackgroundColor: Colors.white,
          buttonColor: Color(0xFF689775),
        ),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            foregroundColor: Color(0xFFDA5144),
            title: Text('CASEWORQER FORUM',
                style: TextStyle(fontFamily: 'Sansita One')),
            centerTitle: true,
          ),
          drawer: new Drawer(
            child: new ListView(
              children: <Widget>[
                new UserAccountsDrawerHeader(
                  accountName: new Text("D01"),
                  accountEmail: new Text("D01@gmail.com"),
                  currentAccountPicture: new CircleAvatar(
                    backgroundColor: Color(0xFF689775),
                    child: new Text("D01"),
                  ),
                ),
                new ListTile(
                    title: new Text("Home"),
                    trailing: new Icon(Icons.home_rounded)),
                new ListTile(
                  title: new Text("Cari Lowongan"),
                  trailing: new Icon(Icons.search_outlined),
                ),
                new ListTile(
                  title: new Text("Buka Lowongan"),
                  trailing: new Icon(Icons.work),
                ),
                new ListTile(
                    title: new Text("Tips Karier"),
                    trailing: new Icon(Icons.grading_sharp)),
                new ListTile(
                  title: new Text("Forum"),
                  trailing: new Icon(Icons.chat_outlined),
                ),
                new Divider(),
                new ListTile(
                  title: new Text("Keluar"),
                  trailing: new Icon(Icons.logout),
                ),
                new ListTile(
                  title: new Text("Close"),
                  trailing: new Icon(Icons.close_outlined),
                  onTap: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          ),
          body: Center(
            child: Container(
                width: 500,
                height: 700,
                child: Column(
                  children: <Widget>[
                    Text('\n'),
                    Row(
                      children: <Widget>[
                        Text('     '),
                        Container(
                          width: 155,
                          height: 40,
                          child: RaisedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('Back to The Forum',
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ],
                    ),
                    Text('\n'),
                    Container(
                      width: 300,
                      height: 100,
                      child: TextField(
                        controller: field,
                        onChanged: (text) {
                          print("Title : $text");
                        },
                        decoration: InputDecoration(
                          fillColor: Colors.black12,
                          filled: true,
                          hintText: 'What\' Your Forum Title ?',
                          labelText: 'Post Title',
                          labelStyle: TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                        maxLines: 2,
                      ),
                    ),
                    Container(
                      width: 300,
                      height: 100,
                      child: TextField(
                        controller: field,
                        onChanged: (text) {
                          print("Message : $text");
                        },
                        decoration: InputDecoration(
                            fillColor: Colors.black12,
                            filled: true,
                            hintText: 'Write Your Form Here ... ',
                            labelText: 'Post Message',
                            labelStyle: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                        maxLines: 12,
                      ),
                    ),
                    Text('\n'),
                    Text('\n'),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: 123,
                            height: 40,
                            child: RaisedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: Color(0xFF34ae57),
                                child: Text('Post to Forum',
                                    style: TextStyle(color: Colors.white))),
                          ),
                          Text('  '),
                          Container(
                            width: 75,
                            height: 40,
                            child: RaisedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: Color(0xFFe93322),
                                child: Text('Cancel',
                                    style: TextStyle(color: Colors.white))),
                          ),
                          Text('  '),
                          Container(
                            width: 70,
                            height: 40,
                            child: RaisedButton(
                                onPressed: () {},
                                color: Color(0xFF918e8e),
                                child: Text('Reset',
                                    style: TextStyle(color: Colors.white))),
                          ),
                          Text('   '),
                        ])
                  ],
                )),
          ),
        ));
  }
}

const MaterialColor white = const MaterialColor(
  0xFFFFFFFF,
  const <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFFFFFFFF),
    200: const Color(0xFFFFFFFF),
    300: const Color(0xFFFFFFFF),
    400: const Color(0xFFFFFFFF),
    500: const Color(0xFFFFFFFF),
    600: const Color(0xFFFFFFFF),
    700: const Color(0xFFFFFFFF),
    800: const Color(0xFFFFFFFF),
    900: const Color(0xFFFFFFFF),
  },
);

const MaterialColor primaryBlack = MaterialColor(
  _blackPrimaryValue,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    400: Color(0xFF000000),
    500: Color(_blackPrimaryValue),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    900: Color(0xFF000000),
  },
);
const int _blackPrimaryValue = 0xFF000000;
