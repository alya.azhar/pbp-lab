import 'package:flutter/material.dart';
import 'AddForum.dart';

void main() {
  runApp(MaterialApp(
    title: 'CASEWORQER',
    home: ForumPage(),
    theme: ThemeData(
      primaryColor: primaryBlack,
      scaffoldBackgroundColor: Colors.white,
      buttonColor: Color(0xFF689775),
    ),
  ));
}

class ForumPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        foregroundColor: Color(0xFFDA5144),
        title: Text('CASEWORQER FORUM',
            style: TextStyle(fontFamily: 'Sansita One')),
        centerTitle: true,
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("D01"),
              accountEmail: new Text("D01@gmail.com"),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Color(0xFF689775),
                child: new Text("D01"),
              ),
            ),
            new ListTile(
                title: new Text("Home"),
                trailing: new Icon(Icons.home_rounded)),
            new ListTile(
              title: new Text("Cari Lowongan"),
              trailing: new Icon(Icons.search_outlined),
            ),
            new ListTile(
              title: new Text("Buka Lowongan"),
              trailing: new Icon(Icons.work),
            ),
            new ListTile(
                title: new Text("Tips Karier"),
                trailing: new Icon(Icons.grading_sharp)),
            new ListTile(
              title: new Text("Forum"),
              trailing: new Icon(Icons.chat_outlined),
            ),
            new Divider(),
            new ListTile(
              title: new Text("Keluar"),
              trailing: new Icon(Icons.logout),
            ),
            new ListTile(
              title: new Text("Close"),
              trailing: new Icon(Icons.close_outlined),
              onTap: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 700,
          child: Column(
            children: <Widget>[
              Text('\n'),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: 125,
                    height: 40,
                    child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddForumPage()));
                        },
                        color: Color(0xFF689775),
                        child: Text('Add Forum',
                            style: TextStyle(color: Colors.white))),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

const MaterialColor primaryBlack = MaterialColor(
  _blackPrimaryValue,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    400: Color(0xFF000000),
    500: Color(_blackPrimaryValue),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    900: Color(0xFF000000),
  },
);
const int _blackPrimaryValue = 0xFF000000;

const MaterialColor white = const MaterialColor(
  0xFFFFFFFF,
  const <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFFFFFFFF),
    200: const Color(0xFFFFFFFF),
    300: const Color(0xFFFFFFFF),
    400: const Color(0xFFFFFFFF),
    500: const Color(0xFFFFFFFF),
    600: const Color(0xFFFFFFFF),
    700: const Color(0xFFFFFFFF),
    800: const Color(0xFFFFFFFF),
    900: const Color(0xFFFFFFFF),
  },
);
