from django.http import response
from django.http.request import HttpRequest
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from .forms import FriendForm
from .models import Friend

mhs_name = 'Azhar' 
npm = 2006462720  

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'name':mhs_name, 'friends':friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm
    response1 = {'form':form}
    if request.method == 'POST':
        friend_form = FriendForm(request.POST)
        if friend_form.is_valid():
            friend_form.save()
            return redirect('/lab-3')
    return render(request, 'lab3_add_friend.html',response1)

