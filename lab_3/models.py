from django.db import models
from datetime import date, datetime
from django.db.models.fields import DateField

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField(max_length=10)
    DOB = models.DateField()