from django.forms import ModelForm
from django.db.models import fields
from .models import Friend

class FriendForm(ModelForm):
    class Meta :
        model = Friend
        fields = '__all__'
