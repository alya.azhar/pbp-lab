import 'package:flutter/material.dart';
import 'AddForum.dart';

void main() {
  runApp(MaterialApp(
    title: 'CASEWORQER',
    home: ForumPage(),
    theme: ThemeData(
      primaryColor: white,
      scaffoldBackgroundColor: Colors.white,
      buttonColor: Color(0xFF689775),
    ),
  ));
}

class ForumPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        foregroundColor: Color(0xFFDA5144),
        title: Text('CASEWORQER FORUM',
            style: TextStyle(fontFamily: 'Sansita One')),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 700,
          child: Column(
            children: <Widget>[
              Text('\n'),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: 125,
                    height: 40,
                    child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddForumPage()));
                        },
                        color: Color(0xFF689775),
                        child: Text('Add Forum',
                            style: TextStyle(color: Colors.white))),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xFF689775),
        type: BottomNavigationBarType.fixed,
        iconSize: 25,
        currentIndex: 4,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Color(0xFFFFFFFF),
            ),
            title: Text('Home', style: TextStyle(color: Color(0xFFFFFFFF))),
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
                color: Color(0xFFFFFFFF),
              ),
              title: Text('Cari Lowongan',
                  style: TextStyle(color: Color(0xFFFFFFFF)))),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.online_prediction_rounded,
              color: Color(0xFFFFFFFF),
            ),
            title: Text('Buka Lowongan',
                style: TextStyle(color: Color(0xFFFFFFFF))),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.stacked_line_chart_sharp,
              color: Color(0xFFFFFFFF),
            ),
            title:
                Text('Tips Karier', style: TextStyle(color: Color(0xFFFFFFFF))),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.chat_rounded,
              color: Color(0xFFFFFFFF),
            ),
            title: Text('Forum', style: TextStyle(color: Color(0xFFFFFFFF))),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.star_half_sharp,
              color: Color(0xFFFFFFFF),
            ),
            title: Text('Company Review',
                style: TextStyle(color: Color(0xFFFFFFFF))),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.login_outlined,
              color: Color(0xFFFFFFFF),
            ),
            title: Text('Masuk', style: TextStyle(color: Color(0xFFFFFFFF))),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.app_registration,
              color: Color(0xFFFFFFFF),
            ),
            title: Text('Daftar', style: TextStyle(color: Color(0xFFFFFFFF))),
          ),
        ],
      ),
    );
  }
}

const MaterialColor primaryBlack = MaterialColor(
  _blackPrimaryValue,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    400: Color(0xFF000000),
    500: Color(_blackPrimaryValue),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    900: Color(0xFF000000),
  },
);
const int _blackPrimaryValue = 0xFF000000;
const MaterialColor white = const MaterialColor(
  0xFFFFFFFF,
  const <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFFFFFFFF),
    200: const Color(0xFFFFFFFF),
    300: const Color(0xFFFFFFFF),
    400: const Color(0xFFFFFFFF),
    500: const Color(0xFFFFFFFF),
    600: const Color(0xFFFFFFFF),
    700: const Color(0xFFFFFFFF),
    800: const Color(0xFFFFFFFF),
    900: const Color(0xFFFFFFFF),
  },
);
