import 'package:flutter/material.dart';

class AddForumPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'CASEWORQER',
        theme: ThemeData(
          primarySwatch: white,
          scaffoldBackgroundColor: Colors.white,
          buttonColor: Color(0xFF689775),
        ),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            foregroundColor: Color(0xFFDA5144),
            title: Text('CASEWORQER FORUM',
                style: TextStyle(fontFamily: 'Sansita One')),
            centerTitle: true,
          ),
          body: Center(
            child: Container(
                width: 500,
                height: 700,
                child: Column(
                  children: <Widget>[
                    Text('\n'),
                    Row(
                      children: <Widget>[
                        Text('     '),
                        Container(
                          width: 155,
                          height: 40,
                          child: RaisedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              // color: Color(0xFF689775),
                              child: Text('Back to The Forum',
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ],
                    ),
                    Text('\n'),
                    Container(
                      width: 300,
                      height: 100,
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: 'What\' Your Forum Title ?',
                          labelText: 'Post Title',
                          labelStyle: TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                        maxLines: 2,
                      ),
                    ),
                    Container(
                      width: 300,
                      height: 100,
                      child: TextField(
                        decoration: InputDecoration(
                            hintText: 'Write Your Form Here ... ',
                            labelText: 'Post Message',
                            labelStyle: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                        maxLines: 12,
                      ),
                    ),
                    Text('\n'),
                    Text('\n'),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: 123,
                            height: 40,
                            child: RaisedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: Color(0xFF34ae57),
                                child: Text('Post to Forum',
                                    style: TextStyle(color: Colors.white))),
                          ),
                          Text('  '),
                          Container(
                            width: 75,
                            height: 40,
                            child: RaisedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: Color(0xFFe93322),
                                child: Text('Cancel',
                                    style: TextStyle(color: Colors.white))),
                          ),
                          Text('  '),
                          Container(
                            width: 70,
                            height: 40,
                            child: RaisedButton(
                                onPressed: () {},
                                color: Color(0xFF918e8e),
                                child: Text('Reset',
                                    style: TextStyle(color: Colors.white))),
                          ),
                          Text('   '),
                        ])
                  ],
                )),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: Color(0xFF689775),
            currentIndex: 4,
            iconSize: 25,
            items: [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  color: Color(0xFFFFFFFF),
                ),
                title: Text('Home', style: TextStyle(color: Color(0xFFFFFFFF))),
              ),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.search,
                    color: Color(0xFFFFFFFF),
                  ),
                  title: Text('Cari Lowongan',
                      style: TextStyle(color: Color(0xFFFFFFFF)))),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.online_prediction_rounded,
                  color: Color(0xFFFFFFFF),
                ),
                title: Text('Buka Lowongan',
                    style: TextStyle(color: Color(0xFFFFFFFF))),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.stacked_line_chart_sharp,
                  color: Color(0xFFFFFFFF),
                ),
                title: Text('Tips Karier',
                    style: TextStyle(color: Color(0xFFFFFFFF))),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.chat_rounded,
                  color: Color(0xFFFFFFFF),
                ),
                title:
                    Text('Forum', style: TextStyle(color: Color(0xFFFFFFFF))),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.star_half_sharp,
                  color: Color(0xFFFFFFFF),
                ),
                title: Text('Company Review',
                    style: TextStyle(color: Color(0xFFFFFFFF))),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.login_outlined,
                  color: Color(0xFFFFFFFF),
                ),
                title:
                    Text('Masuk', style: TextStyle(color: Color(0xFFFFFFFF))),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.app_registration,
                  color: Color(0xFFFFFFFF),
                ),
                title:
                    Text('Daftar', style: TextStyle(color: Color(0xFFFFFFFF))),
              ),
            ],
          ),
        ));
  }
}

const MaterialColor white = const MaterialColor(
  0xFFFFFFFF,
  const <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFFFFFFFF),
    200: const Color(0xFFFFFFFF),
    300: const Color(0xFFFFFFFF),
    400: const Color(0xFFFFFFFF),
    500: const Color(0xFFFFFFFF),
    600: const Color(0xFFFFFFFF),
    700: const Color(0xFFFFFFFF),
    800: const Color(0xFFFFFFFF),
    900: const Color(0xFFFFFFFF),
  },
);
